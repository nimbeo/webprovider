Overview
========

WebProvider is an Android library that allows loading content from
any source into a WebView. It is implemented using a ContentProvider
and handles all the quirks of the Android platform, specially related
to media files (audio and video). Some possible uses:

* Loading encrypted content without decypting it first
* Loading from a zip archive (without decompressing)
* Applying transforms to the content, e.g: removing all links
* And many more...

You can find more Nimbeo projects at our website: [http://nimbeo.com](http://nimbeo.com)

How to use
==========

Copy the class NimbeoWebProvider into your project and subclass from it,
to implement the abstract method ``processFile`` in order to load your
data. Then add your class as a provider in the manifest:

    <provider android:name=".example.NimbeoExampleContentProvider"
        android:authorities="com.nimbeo.contentprovider" />

You should also call ``deleteCache`` on the provider to delete
the temporary files that are created when playing media files.
This is necessary as media files cannot be loaded directly from
memory due to MediaPlayer limitations.

Two examples of use can be found in the repository, loading plain
and encrypted data from the assets directory.

License
=======

Nimbeo WebProvider is licensed under a simplified BSD license: you can use
it for any purpose, as long as the copyright notices is retained.

The included example contains the following track: [Perfect Tomorrow by Mokhov](http://www.jamendo.com/en/track/839330/perfect-tomorrow)