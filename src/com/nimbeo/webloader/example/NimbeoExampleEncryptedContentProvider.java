/*
Copyright (c) 2013, Nimbeo Estrategia e Innovación S.L.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

package com.nimbeo.webloader.example;

import java.io.InputStream;
import java.io.OutputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.net.Uri;
import android.util.Base64;

import com.nimbeo.webloader.NimbeoContentProvider;

/**
 * Example ContentProvider. Loads encrypted data from the assets directory,
 * decrypting it on the fly.
 * 
 * @author nimbeo
 */
public class NimbeoExampleEncryptedContentProvider extends NimbeoContentProvider {
	private static final String KEY = "fGAycngrRBrnlU_VzsDSofJJREnwyH9RnS9lgjd0_mo=";
	
	@Override
	protected void processFile(Uri uri, OutputStream output) {
		try {
			//Open the file
			InputStream is = getContext().getAssets().open(uri.getPath().substring(1));
			
			/* Initialize the cipher
			 * Please note that plain AES is unsafe and should never be used
			 * in a production environment.
			 */
			byte[] rawkey = Base64.decode(KEY, Base64.NO_WRAP + Base64.URL_SAFE);
			SecretKeySpec skeySpec = new SecretKeySpec(rawkey, "AES");
	        Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			
			//Copy and decrypt
			byte[] input = new byte[4096];
			int bytesRead;
			while ((bytesRead = is.read(input, 0, 4096)) != -1) {
				byte[] outputb = cipher.update(input, 0, bytesRead);
				if (outputb != null) {
					output.write(outputb);
				}
			}
			output.write(cipher.doFinal());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
