/*
Copyright (c) 2013, Nimbeo Estrategia e Innovación S.L.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

package com.nimbeo.webloader.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebView;

import com.nimbeo.webloader.NimbeoContentProvider;
import com.nimbeo.webloader.R;

public class NimbeoWebProvider extends Activity {
	//Change this boolean to select one of the two examples
	private static final boolean ENCRYPTED = true;
	private static final String CONTENT_PROVIDER = ENCRYPTED ? "com.nimbeo.encryptedcontentprovider" : "com.nimbeo.contentprovider";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		WebView wv = (WebView) findViewById(R.id.webView1);
		//Needed to be able to load from content:// uri's
		wv.getSettings().setAllowContentAccess(true);
		
		wv.loadUrl("content://" + CONTENT_PROVIDER + (ENCRYPTED ? "/web_encrypted.html" : "/web.html"));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Delete the temporary files when closing the application.
	 * You should delete them as soon as possible (when the webview is not in use, whenever
	 * navigating to a different page, etc.).
	 */
	@Override
	protected void onDestroy() {
		NimbeoContentProvider.getNimbeoContentProvider(this, CONTENT_PROVIDER).deleteCache();
	}
}
