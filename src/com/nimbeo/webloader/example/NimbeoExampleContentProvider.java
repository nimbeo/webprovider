/*
Copyright (c) 2013, Nimbeo Estrategia e Innovación S.L.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

package com.nimbeo.webloader.example;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.net.Uri;

import com.nimbeo.webloader.NimbeoContentProvider;

/**
 * Example ContentProvider. Simply loads data from the assets directory
 * 
 * @author nimbeo
 */
public class NimbeoExampleContentProvider extends NimbeoContentProvider {
	@Override
	protected void processFile(Uri uri, OutputStream output) {
		try {
			//Open the file
			InputStream input = getContext().getAssets().open(uri.getPath().substring(1));
			
			//Copy the file to output
			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = input.read(buffer, 0, 4096)) != -1) {
				output.write(buffer, 0 , bytesRead);
			}
		} catch (IOException e) {
		}
	}
}
