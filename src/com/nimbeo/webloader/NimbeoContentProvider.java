/*
Copyright (c) 2013, Nimbeo Estrategia e Innovación S.L.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

package com.nimbeo.webloader;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * ContentProvider to load data for WebView's. The WebView needs to set
 * wv.getSettings().setAllowContentAccess(true);
 * File will be streamed in-memory (using pipes) when possible, falling back
 * to temporary files for media content, due to MediaPlayer limitations.
 * 
 * @author Nimbeo
 */
public abstract class NimbeoContentProvider extends ContentProvider {
	private final String DIRECTORY_NAME = "/nimbeo_temp";

	/**
	 * Reimplement this to load a file, given a URL. OutputStream will be automatically closed
	 * by the NimbeoContentProvider
	 * @param uri The URI to load
	 * @param output The OutputStream to write to
	 */
	protected abstract void processFile(Uri uri, OutputStream output);
	
	/**
	 * Convenience function to get the ContentProvider instance
	 * @param context Android Context
	 * @param authority Name (authorities in the manifest) of the provider
	 * @return
	 */
	public static NimbeoContentProvider getNimbeoContentProvider(Context context, String authority) {
		return (NimbeoContentProvider)context.getContentResolver().acquireContentProviderClient(authority).getLocalContentProvider();
	}

	/**
	 * Clean the cache directory of the ContentProvider. This is used for temporary storage
	 * of media files.
	 */
	public void deleteCache() {
		File cacheDirectory = new File(getContext().getCacheDir()
				.getAbsolutePath() + DIRECTORY_NAME);

		for (File f : cacheDirectory.listFiles()) {
			f.delete();
		}
	}
	
	/**
	 * Open a file. Normal files will be returned using an in-memory pipe, while
	 * media files will be returned using a temporary file, as MediaPlayer only
	 * supports loading from files or the network.
	 */
	@Override
	public ParcelFileDescriptor openFile(Uri uri, String mode)
			throws FileNotFoundException {

		MimeTypeMap mtm = MimeTypeMap.getSingleton();

		String mimeType = mtm.getMimeTypeFromExtension(MimeTypeMap
				.getFileExtensionFromUrl(uri.toString()));

		if (mimeType != null && (mimeType.startsWith("audio") || mimeType.startsWith("video"))) {
			//Media file, return via temporary file
			
			//Create temp directory if it doesn't exists
			File outputDir = new File(getContext().getCacheDir().getAbsolutePath() + DIRECTORY_NAME);
			if (!outputDir.exists()) {
				outputDir.mkdir();
			}

			try {
				//Open output file
				File outputFile = File.createTempFile("media", null, outputDir);
				OutputStream output = new FileOutputStream(outputFile);
				
				//Load the file
				processFile(uri, output);
				
				//Close and return the file
				output.close();
				ParcelFileDescriptor pfd = ParcelFileDescriptor.open(outputFile, ParcelFileDescriptor.MODE_READ_ONLY);
				return pfd;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} else {
			//Load any non-media file in-memory
			return openPipeHelper(uri, mimeType, null, null,
					new PipeDataWriter<String>() {
						@Override
						public void writeDataToPipe(ParcelFileDescriptor output, Uri uri, String mimeType, Bundle opts, String args) {
							FileOutputStream os = new ParcelFileDescriptor.AutoCloseOutputStream(output);
							processFile(uri, os);
						}
					});
		}
	}

	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		return null;
	}

	@Override
	public boolean onCreate() {
		return false;
	}

	@Override
	public Cursor query(Uri arg0, String[] arg1, String arg2, String[] arg3,
			String arg4) {
		return null;
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		return 0;
	}
}
